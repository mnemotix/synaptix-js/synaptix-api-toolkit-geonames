import {
  generateConnectionArgs,
  GraphQLTypeConnectionQuery,
  LinkFilter,
  mergeResolvers,
  QueryFilter,
  SynaptixDatastoreSession
} from "@mnemotix/synaptix.js";
import {geonamesClient} from "../../..";

export class PlaceGraphQLTypeConnectionQuery extends GraphQLTypeConnectionQuery {
  constructor() {
    super({
      description: `
Search for Place 

@see https://www.geonames.org/export/codes.html for feature code details.

Params :
 - citiesOnly   : Only search for ADM3, ADM4, ADM5 feature codes
 - featureCodes : An array of feature codes
 - country      : Only search within a country - ISO-3166 format (FR, EN ...)
 - bbox         : Only search within bounding box - [north,west,south,east]
 - overrideName : Override name with name from alternateNames or toponymName to get the translated one
`,
      extraArgs: 
`citiesOnly: Boolean
 featureCodes: [String]
 country: String
 bbox: [Float]
 overrideName: Boolean
`

    });
  }

  /**
   * @inheritdoc
   */
  generateType(modelDefinition) {
    const baseType = super.generateType(modelDefinition);
    const extraType = this._wrapQueryType(`
      """ Get place by Geo Coors (lat, lng) """
      placeByGeoCoords(lat:Float, lng:Float): PlaceInterface
    `);
    return `
      ${baseType}
      ${extraType}
    `;
  }

  /**
   * @inheritdoc
   */
  generateResolver(modelDefinition) {
    return this._wrapQueryResolver({
      /**
       * @param _
       * @param args
       * @param synaptixSession
       * @return {}
       */
      places: async (_, args, synaptixSession) => {
        let {qs, first, citiesOnly, featureCodes, country, bbox, overrideName} = args;

        let places = await geonamesClient.searchPlaces({qs, limit: first, citiesOnly, featureCodes, country, bbox, overrideName, lang: synaptixSession.getContext().getLang()});

        return synaptixSession.wrapObjectsIntoGraphQLConnection(places || [], args);
      },
      place: (_, {id}, synaptixSession) => {
        return geonamesClient.getPlaceById({
          id: id.replace("geonames:", ""),
          lang: synaptixSession.getContext().getLang()
        })
      },
      placeByGeoCoords: (_, {lat, lng}, synaptixSession) => {
        return geonamesClient.getPlaceByGeoCoords({
          lat,
          lng,
          lang: synaptixSession.getContext().getLang()
        })
      }
    });
  }
}