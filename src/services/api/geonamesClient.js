/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import got from "got";
import {I18nError} from "@mnemotix/synaptix.js";
import diacritics from "diacritics";

const cache = new Map();

class GeonamesClient {
  async get({geonamesId, service, params = {}} = {}) {
    if (!geonamesId) {
      if (!!process.env.GEONAMES_USERNAME) {
        geonamesId = process.env.GEONAMES_USERNAME;
      } else {
        throw new I18nError("You must provide a Geonames ID to request API. Pass it in GEONAMES_USERNAME environment variable to set it globally.")
      }
    }

    params.username = geonamesId;

    const serializedParams = Object.entries(params).reduce((acc, [key, value]) => {
      if (Array.isArray(value)) {
        for (let valueFragment of value) {
          acc.push(`${key}=${valueFragment}`);
        }
      } else {
        acc.push(`${key}=${value}`);
      }
      return acc;
    }, []).join('&');

    const uri = `http://api.geonames.org/${service}?${serializedParams}`;
    if (cache.has(uri)) {
      return cache.get(uri);
    }
    try {
      let response = await got(uri, {retry: 3}).json();

      if (response) {
        cache.set(uri, response);
        return response;
      }
    } catch (error) {
      console.log("error at geonamesClient.get(), maybe the url is invalid or geonamesId doesn't exist. ", {uri, geonamesId});
      console.log(error);
      return null
    }
  }

  /**
   * Fetch Geonames place by an geonamesId. Cache result to save credit.
   *
   * @param {string} id - Geonames ID
   * @param {string} [lang] - Preferred language
   * @param {number} [retry=0] - Retry count
   * @return {GeonamePlace}
   */
  async getPlaceById({id, lang, overrideName} = {lang: "fr", overrideName: false}) {
    let geonamesPlace = await this.get({
      service: "getJSON",
      params: {
        geonameId: id.toString().replace("geonames:", ""), // some ids are int not string. need to cast them to avoid error
        lang
      }
    });
    if (geonamesPlace) {
      return this._normalizePlace(geonamesPlace, lang, overrideName);
    }
  };

  /**
   * Search for Geonames places.
   * Use Geonames /searchJSON API and restrict to "A" feature classes (http://www.geonames.org/export/codes.html)
   *
   * @param {string} qs - Query string
   * @param {string} [lang=fr] - Preferred language
   * @param {number} [limit=10] - Limit results size
   * @param {number} [retry=0] - Retry count
   * @param {boolean} [citiesOnly=false] - Filter on cities.
   * @param {array} [featureCodes] - Filter on feature codes @see https://www.geonames.org/export/codes.html
   * @return {GeonamePlace[]}
   */
  async searchPlaces({qs, lang = "fr", limit = 10, citiesOnly, featureCodes, bbox, overrideName = false}) {
    let filtersParams;

    if (featureCodes) {
      filtersParams = {
        featureCode: featureCodes
      };
    }
    else if (citiesOnly) {
      filtersParams = {
        featureCode: ["ADM3", "ADM4", "ADM5"],
      };
    } else {
      filtersParams = {
        featureCode: ["PCLI", "PCLD", "ADM1", "ADM3", "ADM4", "ADM5", "PPL", "PPLA", "PPLA2", "PPLA3", "PPLA4", "PPLA5"],
      };
    }

    if (bbox && bbox.length === 4) {
      filtersParams = {
        ...filtersParams,
        north: bbox[0],
        west: bbox[1],
        south: bbox[2],
        east: bbox[3]
      }
    }

    let results = await this.get({
      service: "searchJSON",
      params: {
        name_startsWith: diacritics.remove(qs),
        lang,
        searchlang: lang,
        maxRows: limit || 10,
        ...filtersParams
      }
    });

    if (results.geonames) {
      let normalizedPlaces = [];
      for (const place of results.geonames.slice(0, 5) || []) {
        normalizedPlaces.push(await this.getPlaceById({
          id: place.geonameId,
          lang,
          overrideName
        }));
      }
      return normalizedPlaces;
    }
  }

  /**
   * Search for Geonames places.
   * Use Geonames /searchJSON API and restrict to "ADM1" feature code (http://www.geonames.org/export/codes.html)
   *
   * @param {string} lng - Latitude
   * @param {string} lat - Longitude
   * @param {number} [limit=10] - Limit results size
   * @param {string} lang - Preferred language
   * @return {GeonamePlace[]}
   */
  async searchNearByPlaces({lat, lng, limit, lang} = {lang: 'fr', limit: 10}) {
    let results = await this.get({
      service: "findNearbyJSON",
      params: {
        lat,
        lng,
        lang,
        maxRows: limit || 10,
        featureCode: ["ADM1", "ADM4", "ADM5"]
      }
    });

    if (results.geonames) {
      return results.geonames.map((place) => this._normalizePlace(place, lang));
    }
  }

  /**
   * Fetch Geonames place by a geocoordinates. Cache result to save credit.
   *
   * @param {string} id - Geonames ID
   * @param {string} lang - Preferred language
   * @return {GeonamePlace}
   */
  async getPlaceByGeoCoords({lat, lng, lang} = {lang: "fr"}) {
    let results = await geonamesClient.get({
      service: "findNearbyJSON",
      params: {lat, lng, lang, featureCode: ["ADM3", "ADM4", "ADM5"]}
    });

    if (results.geonames?.length > 0) {
      return this._normalizePlace(results.geonames[0], lang);
    }
  }

  /**
   * @param place
   * @param lang
   * @param overrideName if true search name in alternateNames or toponymName to get translated one
   * @return {{countryId}|*}
   * @private
   */
  _normalizePlace(place, lang, overrideName = false) {
    place.id = `geonames:${place.geonameId}`;
    place.uri = `http://www.geonames.org/${place.geonameId}`;

    // replace name. used for example with "nouvelle orléans" who have "New orleans" as place.name
    if (overrideName) {
      // As name_startsWith parameter changes the "name" format,
      // we prefer to use toponymName instead.
      // @see http://www.geonames.org/export/geonames-search.html
      // <quote>
      // 'name' and 'toponymName'
      // The response returns two name attributes. The 'name' attribute is a localized name, the preferred name in the language passed in the optional 'lang'
      // parameter or the name that triggered the response in a 'startsWith' search. The attribute 'toponymName' is the main name of the toponym as displayed
      // on the google maps interface page or in the geoname file in the download. The 'name' attribute is derived from the alternate names.
      // </quote>

      if (place.name === place.countryName) {
        return place
      }

      // try to search for a translated name in `place.alternateNames` because `toponymName` is often not translated
      let translatedNamefound = false;
      if (place.alternateNames && lang) {
        // try to get isPreferredName first
        translatedNamefound =
          place.alternateNames.find(
            (element) => element.lang && element.lang.toLowerCase() === lang.toLowerCase() && element.isPreferredName
          ) ||
          // if isPreferredName not found take the firt in same lang
          place.alternateNames.find((element) => element.lang && element.lang.toLowerCase() === lang.toLowerCase());
      }

      if (translatedNamefound && translatedNamefound.name) {
        place.name = translatedNamefound.name;
      } else if (place.toponymName) {
        place.name = place.toponymName;
      }
    }

    if (place.countryId && !place.countryId.includes('geonames:')) {
      place.countryId = `geonames:${place.countryId}`;
    }

    return place;
  }
}

export const geonamesClient = new GeonamesClient();
